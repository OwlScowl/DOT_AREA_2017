﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DOT_AREA_2017.Startup))]
namespace DOT_AREA_2017
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
